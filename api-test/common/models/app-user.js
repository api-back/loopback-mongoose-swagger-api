'use strict';

module.exports = function(Appuser) {
  Appuser.staterara = function(cb) {
    var currentDate = new Date();
    var currentHour = currentDate.getHours();
    var OPEN_HOUR = 6;
    var CLOSE_HOUR = 20;
    var response;
    if (currentHour >= OPEN_HOUR && currentHour < CLOSE_HOUR) {
      response = 'We are open for business.';
    } else {
      response = 'Sorry, we are closed. Open daily from 6am to 8pm.';
    }
    cb(null, response);
  };

  Appuser.remoteMethod(
    'staterara', {
      http: {
        path: '/staterara',
        verb: 'get'
      },
      returns: {
        arg: 'staterara',
        type: 'string'
      }
    },
  );

  // APP USER MODEL METHODS
  /*
  Appuser.disableRemoteMethodByName("upsert");                               // disables PATCH /Appusers
  Appuser.disableRemoteMethodByName("find");                                 // disables GET /Appusers
  Appuser.disableRemoteMethodByName("replaceOrCreate");                      // disables PUT /Appusers
  Appuser.disableRemoteMethodByName("create");                               // disables POST /Appusers

  Appuser.disableRemoteMethodByName("prototype.updateAttributes");           // disables PATCH /Appusers/{id}
  Appuser.disableRemoteMethodByName("findById");                             // disables GET /Appusers/{id}
  Appuser.disableRemoteMethodByName("exists");                               // disables HEAD /Appusers/{id}
  Appuser.disableRemoteMethodByName("replaceById");                          // disables PUT /Appusers/{id}
  Appuser.disableRemoteMethodByName("deleteById");                           // disables DELETE /Appusers/{id}

  Appuser.disableRemoteMethodByName('prototype.__get__accessTokens');        // disable GET /Appusers/{id}/accessTokens
  Appuser.disableRemoteMethodByName('prototype.__create__accessTokens');     // disable POST /Appusers/{id}/accessTokens
  Appuser.disableRemoteMethodByName('prototype.__delete__accessTokens');     // disable DELETE /Appusers/{id}/accessTokens

  Appuser.disableRemoteMethodByName('prototype.__findById__accessTokens');   // disable GET /Appusers/{id}/accessTokens/{fk}
  Appuser.disableRemoteMethodByName('prototype.__updateById__accessTokens'); // disable PUT /Appusers/{id}/accessTokens/{fk}
  Appuser.disableRemoteMethodByName('prototype.__destroyById__accessTokens');// disable DELETE /Appusers/{id}/accessTokens/{fk}

  Appuser.disableRemoteMethodByName('prototype.__count__accessTokens');      // disable  GET /Appusers/{id}/accessTokens/count

  Appuser.disableRemoteMethodByName("prototype.verify");                     // disable POST /Appusers/{id}/verify
  Appuser.disableRemoteMethodByName("changePassword");                       // disable POST /Appusers/change-password
  Appuser.disableRemoteMethodByName("createChangeStream");                   // disable GET and POST /Appusers/change-stream

  Appuser.disableRemoteMethodByName("confirm");                              // disables GET /Appusers/confirm
  Appuser.disableRemoteMethodByName("count");                                // disables GET /Appusers/count
  Appuser.disableRemoteMethodByName("findOne");                              // disables GET /Appusers/findOne

  //Appuser.disableRemoteMethodByName("login");                                // disables POST /Appusers/login
  //Appuser.disableRemoteMethodByName("logout");                               // disables POST /Appusers/logout

  Appuser.disableRemoteMethodByName("resetPassword");                        // disables POST /Appusers/reset
  Appuser.disableRemoteMethodByName("setPassword");                          // disables POST /Appusers/reset-password
  Appuser.disableRemoteMethodByName("update");                               // disables POST /Appusers/update
  Appuser.disableRemoteMethodByName("upsertWithWhere");                      // disables POST /Appusers/upsertWithWhere
  */
};

