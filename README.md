# to do
- Export documentation from POSTMAN.
- Generate documentation of new methods.
- Adjust ttl

# INSTALL
**loopback-cli**

```
npm install -g loopback-cli
lb
``` 

*If you have any issue, try install it by using sudo*

**mongo-db**
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

```
tar -zxvf mongodb-osx-ssl-x86_64-4.0.1.tgz
export PATH=<mongodb-install-directory>/bin:$PATH
```
*Replace <mongodb-install-directory> with the path to the extracted MongoDB archive.*




**mongo-db connector**

```
npm install --save loopback-connector-mongodb
```


# LOOPBACK

**Running API application**
```
~api-test/: node .
```
- *Runs at: http://localhost:3000/*
- *API runs at: http://localhost:3000/api/*
- *Swagger explorer runs at: http://localhost:3000/explorer/*
- *Note: in case you have trouble with ports in use:*

```
lsof -t -i :PORTNUMBER
kill -9 PID
```


# POSTMAN
1. Create env variables i.e. api URL
2. Create subfolders for each child method
3. Send POST data using x-www-form-urlencoded



**Bio**

https://medium.freecodecamp.org/build-restful-api-with-authentication-under-5-minutes-using-loopback-by-expressjs-no-programming-31231b8472ca
https://scotch.io/tutorials/speed-up-your-restful-api-development-in-node-js-with-swagger